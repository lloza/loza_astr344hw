import numpy as np

print """For this part of the homework, we want
to find the smallest amount of people for the 
probability to be greater than 0.5 that two people
in the same group have the same birthday.\n"""

print """Thus, a way to approach this is to have n trials,
N people in a room, and 'count' as the number of times we have
two people with the same birthday. As we increase N, the probability
goes up. As we increase n, the answer converges to the correct
value.\n
Thus, what I want to show is that for N = 23 and for some n, the
probability, count/n, < 0.5.\n\n"""

def P(N, n):
	x = []
	count = 0.0
	for i in range(n):
		for j in range(N):
			z = np.random.random_integers(1, 365)
			x.append(z)
		duplicate = set(x)
		if len(duplicate) != len(x):
			count = count + 1.0
		x = []
		duplicate = []
	print count/n
	print "\n"

print "For 22 people with 10,000 trials, the probability is the first value below."
print "For 23 people, with 10,000 trials, we see the probability > 50%.\n\n"
P(22, 10000)
P(23, 10000)