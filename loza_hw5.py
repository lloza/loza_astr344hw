import numpy as np
from math import pi
from math import isinf
import matplotlib.pyplot as plt

print """For hw5, we want to write a 
bisection function that gives us the roots
of any function given.\n"""

print """First I will define
the functions we want to find the 
roots for. Then, once the roots have
been found, I'll plot each function
to make sure the answer is accurate.\n"""

def f(x):
	# Roots from 2,4 is 3.14
	return np.sin(x)

def g(x):
	# Roots from 1,2 is 1.52138
	return (x**3) - x - 2

def y(x):
	# Roots from 0,5 is 2
	return -6 + x + (x**2)

print """The functions have now been
defined in the code. Now to define the
bisection function.\n
-----------------------------------------------------
\n"""

print "The bisection function is now defined. Now to begin root searching...\n"
def bisection(x1, x2, h):
	tol = -10.0**(-4)
	mid = (x1 + x2)/2.0
	while h(mid) < tol or abs(h(mid)) > abs(tol):
		root = h(mid)
		if h(x1)/h(mid) < 0:
				x2 = mid
		else:
			x1 = mid
		mid = (x1+x2)/2.0
	print "The values for f(x) = 0 is x1 = %r and x2 = %r.\n" % (x1, x2)
	
bisection(2.0, 4.0, f)

#print """The values for f(x) = sin(x)
#are x1: %r.\n""" % x1

bisection(1.0, 2.0, g)

#print """The values for g(x) = x**3 - x - 2
#are x1: %r, x2: %r, and root: %r\n""" % (a, b, root)

bisection(0.0, 5.0, y)

#print """The values for y(x) = -6 + x + x**2
#are x1: %r, x2: %r, and root: %r\n""" % (a, b, root)

print """Now to plot each of the functions.
I will do this by making arrays of their
x and f(x) values to plot using matplot.lib.\n"""

print """Below I defined two arrays of x 
and y values for the line x = 0, as to more
accurately see the roots when plotting.\n"""

def drange(start, stop, step):
	r = start
	while r < stop: 
		yield r
		r += step

x0 = []
y0 = []

for i in drange(0, 2*pi, 0.1):
	x0.append(i)
	y0.append(i*0)

print "Below are the arrays for f(x) = sin(x).\n"

f_xvalues = []
f_sinvalues = []

for i in drange(0,2*pi, 0.1):
	f_xvalues.append(i)
	f_sinvalues.append(f(i))

print "Below are the arrays for g(x) = (x**3) -x - 2.\n"

g_xvalues = []
g_yvalues = []

for i in drange(0,2*pi, 0.1):
	g_xvalues.append(i)
	g_yvalues.append(g(i))

print "Below are the arrays for y(x) = -6 + x + (x**2).\n"

y_xvalues = []
y_yvalues = []

for i in drange(0,2*pi, 0.1):
	y_xvalues.append(i)
	y_yvalues.append(y(i))


plt.plot(x0, y0, color = 'black')
plt.plot(f_xvalues, f_sinvalues, color = 'cyan')
plt.plot(g_xvalues, g_yvalues, color = 'magenta')
plt.plot(y_xvalues, y_yvalues, color = 'green')
plt.xlabel('x')
plt.ylabel('f(x)')
plt.axis((1, 4, -2, 2))
plt.show()

print """From looking at the plots as well,
we see that the roots are ~ 3.14, 2.0, and 1.5.
This is accurate with those received from
the bisection method.\n"""