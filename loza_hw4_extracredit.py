from math import sqrt

print """We want to write different integrator functions
to integrate the given function, f(x) = x^3 + 2x^2  - 4.
All of the methods are shown, with the first being
the piecewise linear method.\n"""

print "But first, I will define the function below as f (seen in code.)\n"

def f(x):
	return ((x**3) + (2*x**2) - 4)

print "Now to define the piecewise integrator, also seen in the code.\n"

def piecewise(x1, x2, f1):
	return (x2 - x1)*f1

print """Now that I have defined
the integrator, I have to use it on 
f(x) to get within 1% of -6.66667.
I will try doing steps of 0.01 from
-1 to 1.\n"""

print """However, to do steps of 0.01, I needed
to define a function that would let me do that.
It is defined in the code as drange.\n"""

area = 0

def drange(start, stop, step):
	r = start
	while r < stop: 
		yield r
		r += step

for i in drange(-1, 1, 0.01):
	i = i * 1
	#print i
	area = piecewise(i, i + 0.01, f(i)) + area

print area
print "\n"

print """The value received is -6.6766. It is within 
0.148% of the given value. Thus my integrator 
works! :)\n"""

print """For the trapezoidal integrator, I will just
use the one from hw4 for this new function f(x)
with 0.01 steps.\n"""

def trapezoidal_int(x1, x2, f1, f2):
	return ( (x2 - x1) * ((f2 + f1)/2.0) )

trap_area = 0

for i in drange(-1, 1, 0.01):
	i = i * 1
	trap_area = trapezoidal_int(i, i+0.01, f(i), f(i+0.01)) + trap_area

print trap_area
print "\n"

print """Thus, the value from the trapezoidal integrator
is -6.6666, which is within 0.00105% of the given value. Thus
this integrator also works! :)\n"""


print "Now to begin the Simpson integrator, which can be seen in the code.\n"

def simpson(x1, x2, f0, f1, f2):
	h = x2 - x1
	return h*( (1/3)*f0 +(4/3)*f1 +(1/3)*f2 ) + h**4

print "Since it is now defined, I will use it on f(x), with 0.01 steps.\n"

simpson_area = 0

for i in drange(-1, 1, 0.01):
	i = i * 1
	simpson_area = simpson(i, i + 0.01, f(i), f(i + 0.01), f(i + 0.02)) + simpson_area

print simpson_area
print "\n"

print """Thus, we see my value is -6.656598. This
value is within 0.151% of the given value.
Thus, this integrator also works! :)\n"""

print """Now to begin the Gaussian integrator.
From the text uploaded on Moodle, the Gaussian
has a special case for [-1,1]. For this range, the
integrator is just f(-1/sqrt(3)) + f(1/sqrt(3)).\n"""

#Below was the attempt to define the Gaussian
#integrator, until I realized the special case
#for [-1, 1] was way easier. That is defined below 
#as gaussian_area.


#def gaussian(a, b, f1, f2):
#	w1 = (b-a)/2
#	w2 = (b-a)/2
#	x1 = ((b-a)/2)*((-1)/sqrt(3)) + (b+a)/2
#	x2 = ((b-a)/2)*(1/sqrt(3)) + (b+a)/2
#	return w1*f1 + w2*f2

#gaussian_area = 0

#a = -1
#b = 1
#x1 = ((b-a)/2)*((-1)/sqrt(3)) + (b+a)/2
#x2 = ((b-a)/2)*(1/sqrt(3)) + (b+a)/2

#for i in drange(-1, 1, 0.01):
#	i = i * 1
#	gaussian_area = gaussian(1, 1, f(i), f(i+0.01)) + gaussian_area
w1 = 1
w2 = 1

gaussian_area = w1*f(-1/sqrt(3)) + w2*f(1/sqrt(3))

print """Thus, as we can see, the value from
the Gaussian integrator, -6.66666666667, is
within 4.9*10**(-5)% of the given value. Thus,
our integrator works for the simplified case
of [-1, 1].\n"""