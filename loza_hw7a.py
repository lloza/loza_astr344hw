import numpy as np 
from math import sqrt

print """What we want to do is use the hit 
or miss method to get the value PI. To do this,
my 'amoeba' will be a circle of radius 1.\n"""

print """Below I am defining N, a, b, and z, f(x),
and I. N is the number of random numbers drawn.
a is the lower limit, while b is the upper limit.
z = the Nth random number. f(x) is the function
giving a quarter of the circle. I is approximation 
value. \n"""

print """The answer converges to PI as we increase
N. Below, we have values for N = 1,000, 10,000, 100,000
and 1,000,000, and 10,000,000. It is not until the last N 
value when I get 3.141 (which I think is pretty high, considering
	that's 10e6, which is a lot of zeroes.\n"""

a = 0.0

b = 1.0

def z(N):
	return np.random.random(N)

def f(x):
	return (1.0 - x**2)**0.5

def I(N):
	area = 4*((b-a)/N)*np.sum(f(z(N)))
	print area

I(1000)
I(10000)
I(100000)
I(1000000)
I(10000000)

print "\n"
print """Below in the code is my random number 
generator code. I would have tested it out had I 
known how to make it give only values between 0 and 1
for the example above. Below are 5 random values generated.\n"""

def RNG(seed, m, c, a, N):
	for i in range(N):
		seed = (a*seed + c)%m
		x = seed/m
		print x

RNG(7.0, 10.0**9, 17.0, 26.0, 5)