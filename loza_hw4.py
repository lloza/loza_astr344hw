# What we want to do can be split up in three parts.

# For the first part, we want to write a 
# trapezoidal integrator function. I also am
# going to define a function for r(z) as well as
# Da, as to calculate the values for different z's.

import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
from math import pi
from math import sin

print """For homework 4, we want to write code for a
trapezoidal integrator. Then we want to test this for
angular diameter distance function, by recreating the plot
given. Finally, we want to compare our result to numpy's
np.trapz integrator.\n

Thus, we can split this task up in four parts. First, writing
the functions: the integrator, the r(z), and Da. Second,
we need to attain the (x,y) values, z & Da, to be able to
plot. Third, we plot the data points and compare to the plot
shown. Finally, we use numpy's np.trapz integrator to compare to
our method.\n
------------------------------------------------------------------
\n"""

print "First, we must define the functions.\n"
print """I cannot print them, but they are readable in python.\n
------------------------------------------------------------------
\n"""

def trapezoidal_int(rz1, rz2, z1, z2):
	return ( (z2 - z1) * ((rz2 + rz1)/2.0) )

def rz(z):
	C = (3.0 * (10**3))
	M = 0.3
	A = 0.7
	return ( C / (sqrt(M * ((1 + z**3)) + A)) )



def Da(j, z2):
	return ( j/(1 + z2) )


print """Now calculating the Da values for z = 1, 2, 3,
4, and 5 by iterating in 0.1 increments for each integer.
Then, appending them to a list, along with the z-values, 
for plotting.\n"""

z_values = []
Da_values = []
Da_value = 0
Da_scalar = 0

for i in range(11):
	i = 0.1*i
	x1 = rz(i)
	x2 = rz(i+0.1)
	Da_scalar = trapezoidal_int(x1, x2, i, i + 0.1) + Da_scalar
	Da_value = Da(Da_scalar, i)
	Da_values.append(Da_value)
	z_values.append(i)


#Da_scalar = 0
for i in range(10, 21):
	i = 0.1*i
	x1 = rz(i)
	x2 = rz(i+0.1)
	Da_scalar = trapezoidal_int(x1, x2, i, i + 0.1) + Da_scalar
	Da_value = Da(Da_scalar, i)
	Da_values.append(Da_value)
	z_values.append(i)


#Da_scalar = 0
for i in range(20, 31):
	i = 0.1*i
	x1 = rz(i)
	x2 = rz(i+0.1)
	Da_scalar = trapezoidal_int(x1, x2, i, i + 0.1) + Da_scalar
	Da_value = Da(Da_scalar, i)
	Da_values.append(Da_value)
	z_values.append(i) 


#Da_scalar = 0
for i in range(30, 41):
	i = 0.1*i
	x1 = rz(i)
	x2 = rz(i+0.1)
	Da_scalar = trapezoidal_int(x1, x2, i, i + 0.1) + Da_scalar
	Da_value = Da(Da_scalar, i)
	Da_values.append(Da_value)
	z_values.append(i)


#Da_scalar = 0
for i in range(40, 51):
	i = 0.1*i
	x1 = rz(i)
	x2 = rz(i+0.1)
	Da_scalar = trapezoidal_int(x1, x2, i, i + 0.1) + Da_scalar
	Da_value = Da(Da_scalar, i)
	Da_values.append(Da_value)
	z_values.append(i)

print "Now we have the x and y values (which are z and Da) for plotting. These are lists, shown below.\n"
print z_values
print "\n"
print Da_values
print "\n"

print """I will be plotting these two lists, z and Da(z), against
each other later with the np.trapz plot.\n
------------------------------------------------------------------
\n"""

plt.plot(z_values, Da_values, color = 'cyan')
plt.xlabel('z')
plt.ylabel('Da')
#plt.show()

print """Alternatively, since the plot seen above is
a bit off, I want to show that my trapezoidal integrator works.\n
If doing an integral of Sin(x), from 0 to 2pi, then the integral
should turn out to 0.\n"""

sine_value = 0
x_values = []
y_values = []

for i in range(100):
	i = i * 0.02 * pi
	sine_value = trapezoidal_int(sin(i), sin(i + 0.02 * pi), i, i + 0.02 * pi) + sine_value
	x_values.append(i)
	y_values.append(sine_value)
print "The value I get using the integrator for 100 trapezoids is %r, which is very close to 0." % sine_value
print """\n 
--------------------------------------------------------------------
\n"""
#print x_values
#print "\n \n"
#print y_values

#plt.plot(x_values, y_values)
#plt.show()
print """Now, finally, we will use the np.trapz function
to generate a plot for Da. We want to compare my integrator
function to that of numpy's trapz function.\n"""

print """The way I am using np.trapz, I need to make two arrays
for x and y. The x array is easy, it being the increments of z I will
use (0.05 increments.) However, for the y array, I need to first
make an array of the r(z) values. Then, make an array of np.trapz values,
which I will then use in Da to get the y array. The 0.05 increments means
that there are 100 trapezoids being used in this approximatation.\n"""
print """The cyan plot is the integrator function approximated plot, while
the magenta plot is the np.trapz approximated plot."""

y_values = []
rz_values = []
trapz_values = []
x_values = []

for i in range(51):
	i = i * 0.1
	x_values.append(i)
	y = rz(i)
	rz_values.append(y)
	trapz_values.append(np.trapz(rz_values, x_values))
	Da_value = Da(trapz_values[int(10*i)], i)
	y_values.append(Da_value)

plt.plot(x_values, y_values, color = 'magenta')
plt.show()

print """As we can see from the plots, the np.trapz approximated plot is
way better than my integrator approximated plot. My plot is about 200 ~ 300
off from the np.trapz plot, which isn't too bad. My answer is about 80% close
to the correct answer (taking ~300/1700.) """



