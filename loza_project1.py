#My code is EXTREMELY messy and unorganized, but the general
#outline is split into three portions: first, the initial simulation
#second, inserting vaccinated people
#third, inserting a buffer distance among the people
#the sections are marked off by "------" in the print lines.


import numpy as np
import timeit
from math import sin, cos, sqrt, pi
import matplotlib.pyplot as plt

class person():

	def __init__(self, x, y, speed, infected):
		#x coordinate of person
		self.x = x
		#y coordinate of person
		self.y = y
		#amount person moves by
		self.speed = speed
		#infection status
		self.infected = infected

	def move(self, direction):
		#first move the people
		x1 = self.x + self.speed*cos(direction)
		y1 = self.y + self.speed*sin(direction)
		#now make sure they stay within room
		if x1>1 or self.x<0:
			x1 = self.x - self.speed*cos(direction)
		if y1>1 or self.y<0:
			y1 = self.y - self.speed*sin(direction)
		self.x = x1
		self.y = y1

people = []
infected_people = []
infection_count = 0

def make_people(N, N_infected):
	for i in range(N-N_infected):
		xPosition = np.random.random()
		yPosition = np.random.random()
		speed = 0.02
		people.append(person(xPosition, yPosition, speed, 0))
	for j in range(N_infected):
		xPosition = np.random.random()
		yPosition = np.random.random()
		speed = 0.02
		infected_people.append( person(xPosition, yPosition, speed, 1) )
		people.append( person(xPosition, yPosition, speed, 1))
	#infection_count = len(infected_people)

print len(people)
		
#now to simulate the infecting
times = []
average_times = []
average_time = 0
infection_count = 0
seconds = 0
total_times = 0

#initial infection time function I used,
#comparing computation times vs. seconds
#changes were made to plot the correct graph, 
#but ultimately failed, which resulted in the use
#of the vaccination_time function instead

def infection_time(x, infection_count):
	for z in range(1, x):
		#t0 = timeit.default_timer()
		global seconds
		global total_times
		global average_times
		global average_time
		global times
		seconds = 0
		while infection_count < len(people)-len(infected_people):
			for i in range( len(people) ):
				direction = np.random.random()*2*pi
				people[i].move(direction)

				for j in range( len(infected_people) ):
					dist = sqrt( (people[i].x - infected_people[j].x)**2 + (people[i].y - infected_people[j].y)**2 )
					transmission = np.random.random()

					if dist<0.04 and dist != 0 and transmission<0.75:
						people[i].infected = 1
						infected_people.append(people[i])
			seconds += 1
			if infection_count == len(people)-len(infected_people)-2:
				times.append(seconds)
			infection_count = len(infected_people)
			#print infection_count

		#t1 = timeit.default_timer()
		#time = t1 - t0
		#times.append(time)
	total_times = 0
	for w in range(len(times)):
		total_times = total_times + times[w]
		average_time = total_times/x
		if w == len(times):
			average_times.append(average_time)
	#print average_time

number_of_people = []

#Blanking out the bottom as to continue running
#for later parts of assignment

#Code in stars is what I used to generate
#first batch of plots
#**************************************************************************


people = []
infected_people = []
vaccinated_people = []
infection_count = 0
def make_people_vaccinated(N, N_infected, N_vaccinated):
	for i in range(N-N_vaccinated-N_infected):
		xPosition = np.random.random()
		yPosition = np.random.random()
		speed = 0.05
		people.append(person(xPosition, yPosition, speed, 0))
	for j in range(N_infected):
		xPosition = np.random.random()
		yPosition = np.random.random()
		speed = 0.05
		infected_people.append( person(xPosition, yPosition, speed, 1) )
		people.append( person(xPosition, yPosition, speed, 1) )
	infection_count = len(infected_people)
	for k in range(N_vaccinated):
		xPosition = np.random.random()
		yPosition = np.random.random()
		speed = 0.05
		people.append(person(xPosition, yPosition, speed, 2))
		vaccinated_people.append(person(xPosition, yPosition, speed, 2))

times = []
average_times = []
average_time = 0
seconds = 0
iterations = 0
total_times = 0
def vaccination_time(x, infection_count):
	for z in range(1, x):
		#t0 = timeit.default_timer()
		global average_times 
		global seconds
		global times
		global total_times

		while infection_count < len(people)-len(vaccinated_people):

			for i in range( len(people) ):
				direction = np.random.random()*2*pi
				people[i].move(direction)

				for j in range( len(infected_people) ):
					dist = sqrt( (people[i].x - infected_people[j].x)**2 + (people[i].y - infected_people[j].y)**2 )
					transmission = np.random.random()

					if dist<0.025 and dist != 0 and transmission<0.75 and people[i].infected != 2:
						people[i].infected = 1
						infected_people.append(people[i])

			seconds = seconds + 1.0
			if infection_count == len(infected_people):
				times.append(seconds)
			infection_count = len(infected_people)

	for i in range(len(times)):
		total_times = times[i] + total_times
		print times[i]

		if i == len(times)-1:
			average_time = total_times/(x+0.0)
			average_times.append(average_time)


#number_of_people = []
#for i in range(5, 300, 5):
#	print "Times for %r people below" % i
#	people = []
#	infected_people = []
#	vaccinated_people = []
#	times = []
#	make_people_vaccinated(300, 1, 0)
#	vaccination_time(300, 1)
#	number_of_people.append(i)
#	print "\n"

#for i in range(len(average_times)):
#	print average_times[i]

#print len(average_times)
#print len(number_of_people)

#while len(average_times) != len(number_of_people):
#	number_of_people.pop()

#last_time = average_times[len(average_times)-1] * 1.2
#plt.plot(average_times, number_of_people)
#plt.axis([0.000, last_time, 0, 300])
#plt.xlabel('Time (seconds)')
#plt.ylabel('Number of vaccinated')
#plt.title('Number of vaccinated vs. time taken to infect regular people')
#plt.show()

#for i in range(len(average_times)):
#	print average_times[i]
#**********************************************************************************************
print """\nNow to begin the second part of the assignment, which is
vaccinating people. I will simply redefine the function used above,
and use infected = 2 to indicate vaccinated people.\n
--------------------------------------------------------------------------------------------------------------------------------------
\n"""

people = []
infected_people = []
vaccinated_people = []
infection_count = 0
def make_people_vaccinated(N, N_infected, N_vaccinated):
	for i in range(N-N_vaccinated-N_infected):
		xPosition = np.random.random()
		yPosition = np.random.random()
		speed = 0.05
		people.append(person(xPosition, yPosition, speed, 0))
	for j in range(N_infected):
		xPosition = np.random.random()
		yPosition = np.random.random()
		speed = 0.05
		infected_people.append( person(xPosition, yPosition, speed, 1) )
		people.append( person(xPosition, yPosition, speed, 1) )
	infection_count = len(infected_people)
	for k in range(N_vaccinated):
		xPosition = np.random.random()
		yPosition = np.random.random()
		speed = 0.05
		people.append(person(xPosition, yPosition, speed, 2))
		vaccinated_people.append(person(xPosition, yPosition, speed, 2))

times = []
average_times = []
average_time = 0
seconds = 0
iterations = 0
total_times = 0
def vaccination_time(x, infection_count):
	for z in range(1, x):
		#t0 = timeit.default_timer()
		global average_times 
		global seconds
		global times
		global total_times

		while infection_count < len(people)-len(vaccinated_people):

			for i in range( len(people) ):
				direction = np.random.random()*2*pi
				people[i].move(direction)

				for j in range( len(infected_people) ):
					dist = sqrt( (people[i].x - infected_people[j].x)**2 + (people[i].y - infected_people[j].y)**2 )
					transmission = np.random.random()

					if dist<0.025 and dist != 0 and transmission<0.75 and people[i].infected != 2:
						people[i].infected = 1
						infected_people.append(people[i])

			seconds = seconds + 1.0
			if infection_count == len(infected_people):
				times.append(seconds)
			infection_count = len(infected_people)

	for i in range(len(times)):
		total_times = times[i] + total_times
		print times[i]

		if i == len(times)-1:
			average_time = total_times/(x+0.0)
			average_times.append(average_time)

#number_of_people = []
#for i in range(len(average_times)):
	#print average_times[i]

#Code under stars (in conjunction with defined functions)
#are what I used to make my second main plot
#****************************************************************************
#for i in range(5, 300, 5):
#	print "Times for %r people below" % i
#	people = []
#	infected_people = []
#	vaccinated_people = []
#	times = []
#	make_people_vaccinated(300, 1, i)
#	vaccination_time(300, 1)
#	number_of_people.append(i)
#	print "\n"

#for i in range(len(average_times)):
#	print average_times[i]

#print len(average_times)
#print len(number_of_people)

#last_time = average_times[len(average_times)-1] * 1.2
#plt.plot(average_times, number_of_people)
#plt.axis([0.000, last_time, 0, 300])
#plt.xlabel('Time (seconds)')
#plt.ylabel('Number of vaccinated')
#plt.title('Number of vaccinated vs. time taken to infect regular people')
#plt.show()
#*****************************************************************************

print """\nNow to begin the last part of the assignment, which is
making a buffer between people. I will defined a new 'move' method in my class.\n
------------------------------------------------------------------------------------------------------------------------
\n"""

times = []
average_times = []
average_time = 0
seconds = 0
iterations = 0
total_times = 0
def buffer_time(x, infection_count):
	for z in range(1, x):
		#t0 = timeit.default_timer()
		global average_times 
		global seconds
		global times
		global total_times
		global buffer_distance

		while infection_count < len(people)-len(vaccinated_people):
			for i in range( len(people) ):
				direction = np.random.random()*2*pi
				people[i].move(direction)

				for k in range(len(people)):
					buffer_distance = 0.0125
					buff_dist = sqrt( (people[i].x - people[k].x)**2 + (people[i].y - people[k].y)**2 )
					
					if buff_dist < buffer_distance:
						people[i].move(-direction)
				
				for j in range( len(infected_people) ):
					dist = sqrt( (people[i].x - infected_people[j].x)**2 + (people[i].y - infected_people[j].y)**2 )
					transmission = np.random.random()
					buff_dist = sqrt( (people[i].x - infected_people[j].x)**2 + (people[i].y - infected_people[j].y)**2 )

					if buff_dist < buffer_distance:
						people[i].move(-direction)

					if dist<0.025 and dist != 0 and transmission<0.75 and people[i].infected != 2:
						people[i].infected = 1
						infected_people.append(people[i])

			seconds = seconds + 1.0
			if infection_count == len(infected_people):
				times.append(seconds)
			infection_count = len(infected_people)

	for i in range(len(times)):
		total_times = times[i] + total_times
		print times[i]
		if i == len(times)-1:
			average_time = total_times/(x+0.0)
			average_times.append(average_time)

#for i in range(5, 300, 5):
#	print "Times for %r people below" % i
#	people = []
#	infected_people = []
#	vaccinated_people = []
#	times = []
#	make_people_vaccinated(300, 1, i)
#	buffer_time(300, 1)
#	number_of_people.append(i)
#	print "\n"

#for i in range(len(average_times)):
#	print average_times[i]

#print len(average_times)
#print len(number_of_people)
#while len(average_times) != len(number_of_people):
#	number_of_people.pop()

#last_time = average_times[len(average_times)-1] * 1.2
#plt.plot(average_times, number_of_people)
#plt.axis([0.000, last_time, 0, 300])
#plt.xlabel('Time (seconds)')
#plt.ylabel('Number of vaccinated')
#plt.title('Number of vaccinated vs. time taken to infect people w/ buffer')
#plt.show()
