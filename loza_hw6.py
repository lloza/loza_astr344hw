import numpy as np
from math import exp
import timeit

print """There are two parts to this 
assignment. The first part requires us to 
compare the computing time between root 
finding methods. Thus, most of below is 
defining the old root finding method, the 
differentiator, and the NR method.\n
-----------------------------------------------------
"""

#Below just defining the bisection method from HW5.

def bisection(x1, x2, h):
	tol = -10.0**(-4)
	mid = (x1 + x2)/2.0
	while h(mid) < tol or abs(h(mid)) > abs(tol):
		root = h(mid)
		if h(x1)/h(mid) < 0:
				x2 = mid
		else:
			x1 = mid
		mid = (x1+x2)/2.0
	print "The values for f(x) = 0 is x1 = %r and x2 = %r.\n" % (x1, x2)

#Below just defining the differentiator function necessary for the NR method.

def derivative(j):
	def dj(x1, x2=0.1e-5):
		return (j(x1 + x2/2) - j(x1 - x2/2))/x2
	return dj

#Below just defining the test functions from HW5.

def f(x):
	# Roots from 2,4 is 3.14
	return np.sin(x)

def g(x):
	# Roots from 1,2 is 1.52138
	return (x**3) - x - 2

def y(x):
	# Roots from 0,5 is 2
	return -6 + x + (x**2)

#Below defining the NR method. The tolerance is also defined as tol. 

tol = 10.**(-3)

def NewtonRaphson(h, x1, tolerance):
 	dh = derivative(h)
 	root = x1 - h(x1)/dh(x1)
 	while abs(h(root)) > tolerance:
 		x1 = x1 - h(x1)/dh(x1)
 		root = x1 - h(x1)/dh(x1)
 		print root
 	print "f(x) = 0 at the point x = %r\n" % root

print """Below I will test my NR function against
the functions from HW5. The correct answers are
pi (3.14159), 1.52138, and 2.0000.\n"""

NewtonRaphson(f, 4.0, tol)
NewtonRaphson(g, 2.0, tol)
NewtonRaphson(y, 5.0, tol)

print "-----------------------------------------------------\n"

print """Now I am figuring out how long it takes 
each method to run. On the HW6 page, it suggests we 
use the datetime function, but the timeit module works
much better and to more decimal places as well.
I'll be starting with the bisection method.\n\n"""

t1 = timeit.default_timer()
bisection(2.0, 4.0, f)
bisection_time = timeit.default_timer() - t1
print """It takes the bisection method %r 
seconds to complete the calculation.\n\n""" % bisection_time
print "Now to time the NR method.\n"

t2 = timeit.default_timer()
NewtonRaphson(f, 4, tol)
NR_time = timeit.default_timer() - t2
print """It took the NR method %r 
seconds to complete the calculation. The 
answer changes every time you re-run the
code, but generally, the NR method is
faster.\n
------------------------------------------------------\n
Now to begin finding the temperature. First, 
I will define the Planck function. Then I will 
use the NR method to find the temperature.\n""" % NR_time

def Planck(T):
	c = 2.99 * 10**10
	h = 6.626 * 10**(-27)
	k = 1.38 * 10**(-16)
	v = (c / 0.087)
	return (((2.0 * h * (v**3))/(c**2))/(exp((h*v)/(k*T)) - 1.0) - 1.25 * 10**(-12))

#Since I know that the correct T is 42 K, I used 50 K as the estimate number.

NewtonRaphson(Planck, 50.0, tol)

print """Thus, we see that the T that
corresponds to Bv = 1.25e-12 is 42 K,
which is indeed the correct answer."""

