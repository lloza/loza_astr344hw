#There are three main steps to this code. The first 
# will be to convert the data into lists.

print """ This code is meant to convert
a model number of counts to a differential
number of counts. Also, in doing this,
plotting the model data against the observed
data.\n"""

print """ However, before we begin any of
this, we need to convert the data into lists
for manipulation. The lists are printed
below. \n \n \n """

import numpy as np
import matplotlib.pyplot as plt
from math import log10
x = np.loadtxt('model_smg.dat')
y = np.loadtxt('ncounts_850.dat')
	
# To convert the columns to lists,
# I am appending each value for the first
# column by iterating i[0]

luminosity =[]

for i in x:
	luminosity.append(i[0])
print "We have the first list, the luminosity, which is: \n \n %r" % luminosity
print "\n"

obs_luminosity = []

for i in y:
	obs_luminosity.append(i[0])
print "Now, we have the second list, the observed log(luminosity), which is: \n \n %r" % obs_luminosity
print obs_luminosity
print "\n"

# similarly iterating but with i[1], for
# the second list within the data

ncounts =  []

for i in x:
	ncounts.append(i[1])
print "Now we have the third list, the number of galaxies, which is: \n \n %r" % ncounts
print ncounts
print "\n"

obs_count = []
for i in y:
	obs_count.append(i[1])
print "Now we have the fourth list, the log(dN/dL), which is: \n \n %r" % obs_count
print obs_count
print "\n"

#The second main step is to convert the number counts
# to differential counts. Since each factor has the same 
# i value, by iterating through different i values, 
# I was able to get all variables in one.

dcounts = []

for i in range(1, 11):
	h1 = luminosity[i] - luminosity[i-1]
	h2 = luminosity[i+1] - luminosity[i]
	diff_calculation = log10(-((h1/(h2*(h1+h2)))*ncounts[i+1] - ((h1-h2)/(h1*h2))*ncounts[i] - (h2/(h1*(h1+h2)))*ncounts[i-1]))
	dcounts.append(diff_calculation)
print """Since we have already converted the data into lists,
 we have to now convert the model data into differential counts.
 The formula is iterated through the 10 model data points.
 The differential number of counts is: \n \n %r """ % dcounts


# The third and last main step is to now 
# to make the plot log(dN/dL) vs. log(L)
# and the observed data.

# Taking the log of dL/dN to match the observed data.

print "\n"
log_luminosity = []

for i in x:
	log_luminosity.append(log10(i[0]))
print """Since we have everything and are
 now ready to plot, I am taking the
 log(luminosity) to plot against the
 observed data: \n \n %r""" % log_luminosity

# Now plotting the model data over the observed data

plt.plot(obs_luminosity, obs_count, color = 'yellow')
plt.ylabel('log(dN/dL)')
plt.xlabel('log(Luminosity)')

plt.plot(log_luminosity[1:-1], dcounts, color = 'red')
plt.ylabel('log(dN/dL)')
plt.xlabel('log(Luminosity)')
plt.show()
plt.savefig('loza_hw3.jpg')