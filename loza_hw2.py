# Comments
import numpy as np
x = np.float64(1.e200)
y = np.float32(1.e30)

# What we want to do is compute the absolute and relative errors at
# values of n = 5 and n = 20 for both single precision and double
# precision floating point numbers.

#Below is calculating the recurrence relation for n = 5

print "This is the recurrence relation for single and double precision, respectively.\n"

for i in range(6):
	g = np.float32((1 / 3.) **  i)
	print g
	
print "\n"
	
for i in range(6):
	f = np.float64((1 / 3.) ** i)
	print f
	
print "\n"
# Below are calculations of the errors for n = 5.
	
print "Now to calculate the absolute and relative errors.\n"

absolute = np.float64(f - g)
relative = np.float64((f - g) / f)

print "The absolute error for n = 5 is %r" % absolute
print "\n"
print "The relative error for n = 5 is %r" % relative
print "\n"
# Below are calculations for the recurrence relation for n = 20.

for i in range(21):
	g = np.float32((1 / 3.) **  i)
	
for i in range(21):
	f = np.float64((1 / 3.) ** i)
	
absolute = np.float64(f - g)
relative = np.float64((f - g) / f)
	
# Below are calculations for the errors for n = 20.
	
print "The absolute error for n = 20 is %r" % absolute
print "\n"
print "The relative error for n = 20 is %r" % relative
print "\n"
# Now we want the same for a recurrence relation of 4**n. 

print "Now we have the second recurrence relation, 4^n, for single and double precision respectively.\n"

for i in range(6):
	g = np.float32((4) **  i)
	print g
print "\n"
for i in range(6):
	f = np.float64((4) ** i)
	print f
print "\n"
g = np.float32((4) ** 20)
f = np.float64((4) ** 20)
print "These are the values at n = 20.\n"
print g
print f

#Below is calculation of the errors and an explanation for the stable behavior.

absolute = np.float64(f - g)
relative = np.float64((f - g) / f)
print "\n"
print """This calculation is stable because it works purely with integers,
where the precision of the decimal places does not matter. \n"""
print "The absolute and relative errors are %r and %r, respectively." % (absolute, relative)
print "\n"
print """For this particular case, the relative and absolute errors should
not be used to measure accuracy and stability. The calculation will
remain stable and errors would be 0 as seen above, since there are no 
decimal places at each of the values.\n"""